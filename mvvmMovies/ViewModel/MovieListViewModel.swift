//
//  MovieListViewModel.swift
//  mvvmMovies
//
//  Created by Wesley AIS on 26/04/20.
//  Copyright © 2020 wb. All rights reserved.
//

import Foundation
import RxSwift
import Moya

final class MovieListViewModel {
    let title = "Filmes"
    
    private let movieService: MovieServiceProtocol
    
    init (movieService: MovieServiceProtocol = MovieService()) {
        self.movieService = movieService
    }
    
    func fetchMovieViewModels() -> Observable<[MovieViewModel]> {
        movieService.fetchMovies().map {
            let list = $0
            return $0.list.map {
                MovieViewModel(movie: $0, movieList: list)
            }
        }
    }
}
