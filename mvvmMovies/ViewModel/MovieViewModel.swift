//
//  MovieViewModel.swift
//  mvvmMovies
//
//  Created by Wesley AIS on 26/04/20.
//  Copyright © 2020 wb. All rights reserved.
//

import Foundation

struct MovieViewModel {
    
    private let movie: Movie
    private let movieList: MovieList
    
    var displayTitle: String {
        return movie.title
    }
    
    var totalPages: Int {
        return movieList.totalPages
    }
    
    init(movie: Movie, movieList: MovieList) {
        self.movie = movie
        self.movieList = movieList
    }
}

