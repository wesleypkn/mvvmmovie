//
//  ViewController.swift
//  mvvmMovies
//
//  Created by Wesley AIS on 26/04/20.
//  Copyright © 2020 wb. All rights reserved.
//

import UIKit
import RxSwift
//import RxCocoa
import Moya

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let disposeBag = DisposeBag()
    let movieProvider = MoyaProvider<MovieProvider>()
    private var movieViewModel = MovieListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        let result = movieViewModel.fetchMovieViewModels()
        result.map { (moviesmodel) -> MovieListViewModel in
            print(moviesmodel)
        }
        print(result)
//        movieViewModel.fetchMovieViewModels().observeOn(MainScheduler.instance).bind(to: tableView.rx.items(cellIdentifier: "cell")) { (index, viewModel, cell) in
//            cell.textLabel?.text = viewModel.displayTitle
//        }.disposed(by: disposeBag)
    }


}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
