//
//  MovieProvider.swift
//  mvvmMovies
//
//  Created by Wesley AIS on 26/04/20.
//  Copyright © 2020 wb. All rights reserved.
//

import Foundation
import Moya

enum MovieProvider {
    case getMovies(page: Int)
    case getMoviesDetail(movieId: Int)
    case getMoviesTrailer(movieId: Int)
}

extension MovieProvider: TargetType {
    var baseURL: URL {
        switch self {
        case .getMovies(let page):
            return URL(string:"\(MovieProvider.baseUrl)\(MovieProvider.movie)\(MovieProvider.movieNowPlaying)\(MovieProvider.API)\(MovieProvider.language)&page=\(page)")!
        case .getMoviesDetail(let movieId):
            return URL(string:"\(MovieProvider.baseUrl)\(MovieProvider.movie)/\(movieId)\(MovieProvider.API)\(MovieProvider.language)")!
        case .getMoviesTrailer(let movieId):
            return URL(string:"\(MovieProvider.baseUrl)\(MovieProvider.movie)/\(movieId)\(MovieProvider.API)\(MovieProvider.language)")!
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    
    /// PList abstraction
    static let baseUrl: String = getPlist(variable: "url")
    static let API: String = getPlist(variable: "API")
    static let language: String = getPlist(variable: "language")
    static let movie: String = getPlist(variable: "movie")
    static let videos: String = getPlist(variable: "videos")
    static let search: String = getPlist(variable: "search")
    static let movieNowPlaying: String = getPlist(variable: "movieNowPlaying")
    static let movieUpcoming: String = getPlist(variable: "movieUpcoming")
    static let topRated: String = getPlist(variable: "topRated")
    static let popular: String = getPlist(variable: "popular")
    
    static func getPlist(variable: String) -> String {
        return get(variable: variable)[variable] ?? ""
    }
    
    static func get(variable: String) -> [String : String] {
        if let url = Bundle.main.url(forResource: "service", withExtension: "plist"),
            let values = NSDictionary(contentsOf: url) as? [String:String] {
            return values
        }
        print("PLIST Error")
        return [:]
    }
    
}
