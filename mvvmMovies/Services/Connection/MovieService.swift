//
//  MovieService.swift
//  mvvmMovies
//
//  Created by Wesley AIS on 26/04/20.
//  Copyright © 2020 wb. All rights reserved.
//

import Foundation
import RxSwift
import Moya

protocol MovieServiceProtocol {
    func fetchMovies() -> Observable<MovieList>
}

class MovieService: MovieServiceProtocol {
    
    let movieProvider = MoyaProvider<MovieProvider>()
    
    func fetchMovies() -> Observable<MovieList> {
        return Observable.create { observer -> Disposable in
            self.movieProvider.request(.getMovies(page: 1)) { (result) in
                switch result {
                case .success(let response):
                    do {
                        let object = try JSONDecoder().decode(MovieList.self, from: response.data)
                        observer.onNext(object)
                    } catch {
                        observer.onError(error)
                    }
                case .failure(let error):
                    print(error)
                    observer.onError(error)
                }
            }
            
            return Disposables.create { }
        }
    }
    
}
